const url = 'http://api.tvmaze.com/';

export const searchSeries = (query: string, func: any): void => {
  var full_query = url + 'search/shows?q=' + query;
  console.log('REQUEST: ' + full_query);
  fetch(full_query)
    .then(function(response) {
      response.json().then(function(data) {
        func(data);
      });
    })
    .catch(function(err) {
      func(err);
    });
};

export const searchEpisodes = (id: string, func: any): void => {
  var full_query = url + 'shows/' + id + '/episodes';
  console.log('REQUEST: ' + full_query);
  fetch(full_query)
    .then(function(response) {
      response.json().then(function(data) {
        func(data);
      });
    })
    .catch(function(err) {
      func(err);
    });
};

export const searchDeatils = (
  id: string,
  season: string,
  number: string,
  func: any,
): void => {
  var full_query =
    url +
    'shows/' +
    id +
    '/episodebynumber?season=' +
    season +
    '&number=' +
    number;
  console.log('REQUEST: ' + full_query);
  fetch(full_query)
    .then(function(response) {
      response.json().then(function(data) {
        func(data);
      });
    })
    .catch(function(err) {
      func(err);
    });
};
