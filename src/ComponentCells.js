import React from 'react';
import {
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Text,
} from 'react-native';

export const CellSeries = ({item, index}, func: any) => {
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={() => func(item)}>
      <ImageBackground source={{uri: item.image}} style={cellStyles.cellSeries}>
        <Text style={cellStyles.cellTitle}>{item.name}</Text>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export const CellEpisodes = ({item, index}, func: any) => {
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={() => func(item)}>
      <ImageBackground source={{uri: item.image}} style={cellStyles.cellSeries}>
        <Text style={cellStyles.cellTitle}>
          {item.number} - {item.name}
        </Text>
      </ImageBackground>
    </TouchableOpacity>
  );
};

// Style
const cellStyles = StyleSheet.create({
  cellSeries: {
    backgroundColor: '#ABABAB',
    height: 320,
    marginVertical: 8,
    marginHorizontal: 16,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cellTitle: {
    fontSize: 25,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#FFFFFF',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
  },
});
