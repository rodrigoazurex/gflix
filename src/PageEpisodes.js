// React
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  FlatList,
  Text,
  ImageBackground,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

// Models
import {DataEpisodes, DataInfo} from './ModelObjects';
// Components
import {CellEpisodes} from './ComponentCells';
import {ModalAlert} from './ComponentModal';
// functions
import {searchEpisodes, searchDeatils} from './DataApi';
import {isNullOrEmpty} from './GenericFunctions';

class PageEpisodes extends React.Component {
  static navigationOptions = {
    title: 'Episódios',
  };

  // variables
  state = {
    spinner: false,
    name: '',
    url:
      'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg',
    episodes: [],
    empty:
      'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg',
    info: DataInfo,
  };
  constructor(props) {
    super(props);
    this.props.navigation.addListener('willFocus', () => {
      this.state.id = this.props.navigation.state.params.id;
      this.state.name = this.props.navigation.state.params.name;
      this.state.url = this.props.navigation.state.params.url;
      this.searchEpisodes(this.state.id);
    });
  }

  searchEpisodes(id: any): void {
    this.setState({spinner: true});
    searchEpisodes(id, (result: any) => {
      this.state.episode = [];
      var response = JSON.stringify(result);
      var list = JSON.parse(response);
      list.forEach(episode => {
        var image = episode.image;
        var original = '';
        if (image != null) {
          original = image.original;
          original = original.replace('http:', 'https:');
        }
        this.state.episodes.push(
          new DataEpisodes(
            id,
            episode.id,
            episode.season,
            episode.number,
            episode.name,
            isNullOrEmpty(original) ? this.state.empty : original,
          ),
        );
      });
      this.setState({spinner: false});
    });
  }

  tappedSelected(item: DataEpisodes): void {
    this.setState({spinner: true});
    searchDeatils(item.serie_id, item.season, item.number, (result: any) => {
      var response = JSON.stringify(result);
      var object = JSON.parse(response);
      this.state.info = new DataInfo(object.id, object.name, object.summary);
      this.setState({spinner: false, visible: true});
    });
  }

  // render
  render() {
    return (
      <SafeAreaView style={styles.safearea}>
        <Spinner
          visible={this.state.spinner}
          textStyle={styles.spinnerTextStyle}
        />

        {ModalAlert(
          this.state.visible,
          this.state.info.name,
          this.state.info.summary,
          'Fechar',
          () => {
            this.setState({visible: false});
          },
        )}

        <ImageBackground
          source={{uri: this.state.url}}
          style={styles.headerImage}>
          <Text style={styles.headerTitle}>{this.state.name}</Text>
        </ImageBackground>
        <FlatList
          data={this.state.episodes}
          style={styles.scrollTable}
          renderItem={({item, index}) =>
            CellEpisodes({item, index}, (item: DataEpisodes) => {
              this.tappedSelected(item);
            })
          }
          keyExtractor={item => '' + item.id}
        />
      </SafeAreaView>
    );
  }
}

// Style
const styles = StyleSheet.create({
  safearea: {
    flex: 1,
    backgroundColor: '#CCCCCC',
  },
  headerImage: {
    backgroundColor: '#ABABAB',
    width: '100%',
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: 25,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#FFFFFF',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
  },
  scrollTable: {
    backgroundColor: 'powderblue',
  },
});

// Export class
export default PageEpisodes;
