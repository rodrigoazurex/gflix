// React
import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

// Pages
import PageSearch from './PageSearch';
import PageEpisodes from './PageEpisodes';

class index extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const AppNavigator = createStackNavigator({
  Search: {
    screen: PageSearch,
  },
  Episodes: {
    screen: PageEpisodes,
  },
});

const AppContainer = createAppContainer(AppNavigator);

export default index;
