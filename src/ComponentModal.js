// React
import React from 'react';
import {StyleSheet, Text} from 'react-native';
import Modal, {
  ModalTitle,
  ModalContent,
  ScaleAnimation,
} from 'react-native-modals';
import {Button} from 'react-native-paper';

export const ModalAlert = (
  show = false,
  title = 'Title',
  description = 'Description',
  button = 'Close',
  close: any,
) => {
  return (
    <Modal
      onTouchOutside={() => {
        close();
      }}
      width={0.9}
      visible={show}
      onSwipeOut={() => close()}
      modalAnimation={new ScaleAnimation()}
      onHardwareBackPress={() => {
        console.log('onHardwareBackPress');
        close();
        return true;
      }}
      modalTitle={<ModalTitle title={title} hasTitleBar={false} />}>
      <ModalContent>
        <Text>{description}</Text>
        <Button
          style={modalStyles.button}
          onPress={() => {
            close();
          }}>
          {button}
        </Button>
      </ModalContent>
    </Modal>
  );
};

// Style
const modalStyles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 0,
    overflow: 'hidden',
  },
});
