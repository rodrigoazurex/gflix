// React
import React, {Component} from 'react';
import {StyleSheet, SafeAreaView, View, FlatList, Keyboard} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import {TextInput, Button} from 'react-native-paper';
// Models
import {DataSeries} from './ModelObjects';
// Components
import {CellSeries} from './ComponentCells';
// functions
import {searchSeries} from './DataApi';
import {isNullOrEmpty} from './GenericFunctions';

export default class PageSearch extends Component {
  static navigationOptions = {
    title: 'Busca',
  };

  // variables
  state = {
    spinner: false,
    serie: '',
    enabledSearch: true,
    data: [],
    episodes: [],
    empty:
      'https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg',
  };

  // functions
  onChangeText(serie: string): void {
    this.state.enabledSearch = serie.length === 0;
  }

  tappedSearch(): void {
    Keyboard.dismiss();
    this.setState({spinner: true});
    searchSeries(this.state.serie, (result: any) => {
      this.state.data = [];
      // tratamento do json
      var response = JSON.stringify(result);
      var list = JSON.parse(response);
      // criação da lista
      list.forEach(serie => {
        var image = serie.show.image;
        var original = '';
        if (image != null) {
          original = image.original;
          original = original.replace('http:', 'https:');
        }
        this.state.data.push(
          new DataSeries(
            serie.show.id,
            serie.score,
            serie.show.name,
            isNullOrEmpty(original) ? this.state.empty : original,
            serie.show.rating.average,
          ),
        );
      });
      this.setState({spinner: false});
    });
  }

  tappedSelected(item: DataSeries): void {
    var image = isNullOrEmpty(item.image) ? this.state.empty : item.image;
    var original = image.replace('http:', 'https:');
    this.props.navigation.navigate({
      routeName: 'Episodes',
      params: {
        name: item.name,
        url: original,
        id: item.id,
      },
      key: 'Episodes',
    });
  }

  // render
  render() {
    return (
      <SafeAreaView style={styles.safearea}>
        <Spinner
          visible={this.state.spinner}
          textStyle={styles.spinnerTextStyle}
        />
        <View style={styles.boxButton}>
          <TextInput
            style={styles.textSearch}
            adjustsFontSizeToFit={true}
            label="Série"
            placeholder="Informe a série que deseja"
            value={this.state.serie}
            onChangeText={serie => {
              this.setState({serie});
              this.onChangeText(serie);
            }}
          />
          <Button
            style={styles.buttonSearch}
            mode="contained"
            disabled={this.state.enabledSearch}
            onPress={() => this.tappedSearch()}>
            Buscar
          </Button>
        </View>
        <FlatList
          data={this.state.data}
          style={styles.scrollTable}
          renderItem={({item, index}) =>
            CellSeries({item, index}, (item: DataSeries) => {
              this.tappedSelected(item);
            })
          }
          keyExtractor={item => '' + item.id}
        />
      </SafeAreaView>
    );
  }
}

// Style
const styles = StyleSheet.create({
  safearea: {
    flex: 1,
    backgroundColor: '#CCCCCC',
  },
  boxButton: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 100,
    backgroundColor: 'powderblue',
  },
  textSearch: {
    textAlign: 'center',
    height: 60,
    width: 240,
    fontSize: 15,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 20,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    overflow: 'hidden',
  },
  buttonSearch: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: 120,
    marginTop: 10,
    marginBottom: 10,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    overflow: 'hidden',
  },
  scrollTable: {
    backgroundColor: 'powderblue',
  },
});
