export function isNullOrEmpty(value) {
  return value == null || value === '';
}
