export class DataSeries {
  constructor(id, score, name, image, rating) {
    this.id = id;
    this.score = score;
    this.name = name;
    this.image = image;
    this.rating = rating;
  }
}

export class DataEpisodes {
  constructor(serie_id, id, season, number, name, image) {
    this.serie_id = serie_id;
    this.id = id;
    this.season = season;
    this.number = number;
    this.name = name;
    this.image = image;
  }
}

export class DataInfo {
  constructor(id, name, summary) {
    this.id = id;
    this.name = name;
    this.summary = summary;
  }
}
