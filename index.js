import app from './app.json';
import {AppRegistry} from 'react-native';
import index from './src/PageIndex';
// Importe o package do CodePush
import codePush from 'react-native-code-push';

const codePushOptions = {checkFrequency: codePush.CheckFrequency.ON_APP_RESUME};

AppRegistry.registerComponent(app.name, () => codePush(codePushOptions)(index));
